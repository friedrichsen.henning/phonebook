﻿using Microsoft.Extensions.DependencyInjection;
using PhonebookLib;
using System;
using System.Collections.ObjectModel;
using System.IO.Abstractions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfPhonebook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        private IPhonebook mPhonebook;

        #endregion


        #region PhoneList dependency property

        private static readonly DependencyPropertyKey PhoneListPropertyKey = DependencyProperty.RegisterReadOnly(
            nameof(PhoneList), typeof(ObservableCollection<PhonebookEntry>), typeof(MainWindow),
            new PropertyMetadata(new ObservableCollection<PhonebookEntry>()));


        public static readonly DependencyProperty PhoneListProperty = PhoneListPropertyKey.DependencyProperty;


        private ObservableCollection<PhonebookEntry> PhoneList
        {
            get
            {
                return GetValue(PhoneListProperty) as ObservableCollection<PhonebookEntry>;
            }
        }

        #endregion


        #region Filter dependency property

        private static readonly DependencyProperty FilterProperty = DependencyProperty.Register(
            nameof(Filter), typeof(string), typeof(MainWindow),
            new PropertyMetadata(new PropertyChangedCallback(FilterChanged)));


        private string Filter
        {
            get
            {
                return GetValue(FilterProperty) as string;
            }
            set
            {
                SetValue(FilterProperty, value);
            }
        }


        private static void FilterChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var me = obj as MainWindow;
            me.PopulateList();
        }

        #endregion


        #region Init and clean-up

        public MainWindow()
        {
            // DependencyInjection with default settings
            var services = new ServiceCollection()
                .AddSingleton<IPhonebook, FilePhonebook>()
                .AddSingleton<IFileSystem, FileSystem>()
                .AddSingleton(new FileSettings { Path = FileSettings.DefaultPath })
                .AddSingleton(new SqlSettings())
                .AddSingleton(new FirebaseSettings
                {
                    CredentialsPath = FirebaseSettings.DefaultCredentialsPath,
                    ProjectName = FirebaseSettings.DefaultProjectName
                });

            switch (App.mSource)
            {
                case DatabaseSources.File:
                    services.AddSingleton<IPhonebook, FilePhonebook>();
                    break;

                case DatabaseSources.Sql:
                    services.AddSingleton<IPhonebook, SqlPhonebook>();
                    break;

                case DatabaseSources.Firebase:
                    services.AddSingleton<IPhonebook, FirebasePhonebook>();
                    break;
            }

            mPhonebook = services.BuildServiceProvider().GetService<IPhonebook>();

            InitializeComponent();
        }


        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            PopulateList();
        }

        #endregion


        #region UI handlers

        private void AddButtonClickHandler(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var entry = btn.CommandParameter as PhonebookEntry;
            mPhonebook.Add(new PhonebookEntry
            {
                FirstName = entry.FirstName,
                LastName = entry.LastName,
                BirthYear = entry.BirthYear
            });

            // Re-read
            PopulateList();
        }


        private void DeleteButtonClickHandler(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var entry = btn.CommandParameter as PhonebookEntry;
            mPhonebook.Delete(entry.Id);

            // Re-read
            PopulateList();
        }


        private void ClearFilterButtonClickHandler(object sender, RoutedEventArgs e)
        {
            Filter = "";
        }

        #endregion


        #region Utility

        private void PopulateList()
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                PhoneList.Clear();

                foreach (var entry in mPhonebook.GetList(Filter))
                {
                    PhoneList.Add(entry);
                }

                PhoneList.Add(new InsertPhonebookEntry());
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        #endregion
    }


    public class InsertPhonebookEntry : PhonebookEntry { }
}
