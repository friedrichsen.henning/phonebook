﻿namespace PhonebookLib
{
    public class FileSettings
    {
        #region Constants

        public const string DefaultPath = @"C:\Temp\pb.xml";

        #endregion


        #region Properties

        public string Path { get; set; } = DefaultPath;

        #endregion
    }
}
